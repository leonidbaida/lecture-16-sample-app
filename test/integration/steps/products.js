module.exports = function () {
    var Widget = this.Widget;

    this.Given(/^I visit products page$/, function () {
        return this.driver.get('http://localhost/#products');
    });

    this.Then(/^I should see "([^"]*)" products on the page$/, function (count) {
        return new Widget.ProductsList().items().should.eventually.have.length(count);
    });

    this.Then(/^I should see product "([^"]*)" is "([^"]*)"$/, function (position, state) {
        var available;
        if (state === 'available') {
            available = true;
        } else if (state === 'not available') {
            available = false;
        } else {
            throw 'Unknown state: ' + state;
        }

        if (available) {
            return new Widget.ProductsList().isAvailable(+position).should.eventually.be.true;
        } else {
            return new Widget.ProductsList().isAvailable(+position).should.eventually.be.false;
        }
    });

    this.When(/^I click on product "([^"]*)"$/, function (position) {
        return new Widget.ProductsList().clickTheButton(+position)
    });

    this.Then(/^I see cart is empty$/, function () {
        return new Widget.ProductsCart().getTitle().should.eventually.eql('Cart: Empty');
    });

    this.Then(/^I see "([^"]*)" products in the cart$/, function (count) {
        return new Widget.ProductsCart().getTitle().should.eventually.eql('Cart: ' + count);
    });
};
