var express = require('express');
var router = express.Router();
var Product = require('../mongo/Product');
var Order = require('../mongo/Order');

router.route('/')
    .get(function (req, res) {
        var cart = req.query.cart;

        Product.find({}, function (err, products) {
            if (err) {
                console.error(err);
                return res.sendStatus(500);
            }
            res.json({
                items: cart.map(function (item) {
                    item.product = products.filter(function (product) {
                        return product._id == item.product;
                    })[0];

                    return item
                })
            });
        });
    })
    .post(function (req, res) {
        var order = new Order(req.body);

        order.save(function (err) {
            if (err) {
                console.error(err);
                return res.sendStatus(500);
            }
            res.json({hash: order.hash})
        });
    });

module.exports = router;
