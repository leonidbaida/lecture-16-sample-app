define([
    'backbone.marionette',
    './template'
], function (Backbone, template) {
    return Backbone.LayoutView.extend({
        el: document.body,
        template: template,

        regions: {
            header: '.application-header',
            content: '.application-content',
            footer: '.application-footer'
        }
    });
});